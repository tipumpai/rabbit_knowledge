@extends('master')

@section('title', 'Login')

@section('content')
    @if (session()->has('message'))
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ session('message') }}
        </div>
    @endif

    <form action="/login" method="post" accept-charset="utf-8">
        {{ csrf_field() }}

        <input type="email" name="email" class="form-control" value="{{ old('email') }}">
        <input type="password" name="password" class="form-control">
        <input type="checkbox" name="manual_login" checked> Manual Authentication
        <p></p>

        <input type="submit" value="Login" class="btn btn-primary">
        <input type="reset" value="Cancel" class="btn btn-danger">
    </form>
@endsection