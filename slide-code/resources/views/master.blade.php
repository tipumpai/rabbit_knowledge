<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>@yield('title')</title>
    <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>@yield('title')</h3>
                <hr>
            </div>
        </div>

        <div class="row">
            <div class="col-md-11 col-sm-10">
                <a href="/employees" class="btn btn-default">Employee List</a>
                <a href="/employees/create" class="btn btn-default">New Employee</a>
            </div>

            <div class="col-md-1 col-sm-2">
                @if(auth()->check())
                    <a href="/logout" class="btn btn-danger">Logout</a>
                @endif
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <hr>
                @if (session('message') !== null)
                    <div class="alert alert-info">
                        <strong>Info: </strong> {{ session('message') }}
                    </div>
                @endif
                @section('content')
                    Master Page Content
                @show
            </div>
        </div>
    </div>

    <script type="text/javascript" src="/js/jquery.min.js"></script>
    <script type="text/javascript" src="/js/bootstrap.min.js"></script>
</body>
</html>