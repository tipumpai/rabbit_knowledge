@extends('master')

@section('title', 'Employee Edit')

@section('content')
	<form action="/employees/{{ $employee->id }}" method="post" accept-charset="utf-8">
		{{ csrf_field() }}
		<input type="hidden" name="_method" value="PUT">

		@if (isset($errors) && $errors->has('first_name'))
			<div class="alert alert-danger" role="alert">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				{{ $errors->first('first_name') }}
			</div>
		@endif

		<input type="text" name="first_name" class="form-control" placeholder="Firstname" @if(old('first_name') !== null) value="{{ old('first_name') }}" @else value="{{ $employee->first_name }}"@endif>

		@if (isset($errors) && $errors->has('last_name'))
			<div class="alert alert-danger" role="alert">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				{{ $errors->first('last_name') }}
			</div>
		@endif

		<input type="text" name="last_name" class="form-control" placeholder="Lastname" @if(old('last_name') !== null) value="{{ old('last_name') }}" @else value="{{ $employee->last_name }}"@endif>

		@if(old('gender') == 'M' || $employee->gender == 'M')
			<input type="radio" value="M" name="gender" checked>Male
			<input type="radio" value="F" name="gender">Female
		@else
			<input type="radio" value="M" name="gender">Male
			<input type="radio" value="F" name="gender" checked>Female
		@endif

		<p></p>
		<input type="submit" value="Save" class="btn btn-primary">
		<input type="reset" value="Cancel" class="btn btn-danger">
	</form>
@endsection