@extends('master')

@section('title', 'Employee List')

@section('content')
    <table class="table">
        <thead>
            <tr>
                <th>#</th>
                <th>ID</th>
                <th><a href="/employees?sort=first_name">Firstname</a></th>
                <th><a href="/employees?sort=last_name">Lastname</a></th>
                <th><a href="/employees?sort=gender">Gender</a></th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
        </thead>
        <tbody>
            @foreach($employees as $idx => $e)
               <tr>
                    <td>{{ $idx }}</td>
                    <td><a href="/employees/{{ $e->id}}">{{ $e->id }}</a></td>
                    <td>{{ $e->first_name }}</td>
                    <td>{{ $e->last_name }}</td>
                    <td>{{ genderHelper($e->gender) }}</td>
                    <td>
                        <a href="/employees/{{ $e->id }}/edit" class="btn btn-primary">Edit</a>
                    </td>
                    <td>
                        <form action="/employees/{{$e->id}}" method="post" accept-charset="utf-8">
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="DELETE">

                            <input type="submit" value="Delete" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete?');">
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    {{ $employees->links() }}
@endsection