@extends('master')

@section('title', 'Employee Detail: (' . $employee->first_name . ' ' . $employee->last_name . ')')

@section('content')
    <p>ID: {{ $employee->id }}</p>
    <p>Name: {{ $employee->first_name }}, {{ $employee->last_name }}</p>
    <p>Gender: {{ genderHelper($employee->gender) }}</p>

    @if($employee->salaries->count() > 0)
        <table class="table">
            <thead>
                <tr>
                    <td>From Date</td>
                    <td>To Date</td>
                    <td>Salary</td>
                </tr>
            </thead>

            <tbody>
                @foreach($employee->salaries as $s)
                    <tr>
                        <td>
                            {{ $s->from_date }}
                        </td>
                        <td>
                            {{ $s->to_date }}
                        </td>
                        <td>
                            {{ number_format($s->salary) }}
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endif
@endsection