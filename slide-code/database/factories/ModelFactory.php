<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Employee::class, function (Faker\Generator $faker) {
    $gender = ['M', 'F'];
    return [
        'first_name' => $faker->name,
        'last_name' => $faker->lastName,
        'birth_date' => $faker->dateTimeBetween('-50 years', '-20 years'),
        'hire_date' => $faker->dateTimeBetween('-10 years', '-3 months'),
        'gender' => $gender[rand(0, 1)],
    ];
});