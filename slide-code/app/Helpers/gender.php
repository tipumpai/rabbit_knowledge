<?php

function genderHelper($gender) {
    return ($gender == 'M') ? 'Male' : 'Female';
}