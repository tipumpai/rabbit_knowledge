<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    // return view('welcome');
    return redirect('employees');
});

Route::group(['middleware' => 'web'], function () {
    Route::get('login', 'AuthenController@getLogin');
    Route::post('login', 'AuthenController@postLogin');
    Route::get('logout', 'AuthenController@logout');
});

Route::group(['middleware' => ['web', 'auth']], function () {
    Route::resource('employees', 'EmployeeController');
});

Route::group(['prefix' => 'api', 'middleware' => ['api', 'auth:api', 'cors']], function () {
    Route::options('employees', ['middleware' => 'cors', function () {
        $headers = [
            'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ];

        return response(null, 200, $headers);
    }]);

    Route::resource('employees', 'Api\EmployeeController', [
        'except' => ['create', 'edit']
    ]);
});
