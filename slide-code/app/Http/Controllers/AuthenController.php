<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class AuthenController extends Controller
{
    public function getLogin()
    {
        return view('authens.login');
    }

    public function postLogin()
    {
        $credentials = \Request::all();
        $success = false;
        unset($credentials['_token']);

        if(isset($credentials['manual_login']) && $credentials['manual_login'] == 'on') {
            $user = \App\User::where('email', $credentials['email'])
                ->first();

            if (\Hash::check($credentials['password'], $user->password)) {
                auth()->login($user);

                $success = true;
            }
        } else {
            if (auth()->attempt($credentials)) {
                $success = true;
            }
        }

        if ($success) {
            return redirect()->intended('employees');
        } else {
            session()->flash('message', 'Email or password is incorrect');
            return redirect()->back()
                ->withInput(request()->except(['password']));
        }
    }

    public function logout()
    {
        auth()->logout();

        return redirect('/');
    }
}
