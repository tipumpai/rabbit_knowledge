<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class EmployeeController extends Controller
{

    private $indexPage = 'employees';

    private $rules = [
        'first_name' => 'required|min:5',
        'last_name' => 'required|min:5'
    ];

    private $messages = [
        'required' => 'Ohhhh !!!'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sort = request()->query();

        if (isset($sort['sort'])) {
            $employees = \App\Employee::orderBy($sort['sort'])
                ->paginate(10);
        } else {
            $employees = \App\Employee::paginate(10);
        }

        return view('employees.index', compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('employees.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $employee = request()->all();
        unset($employee['_token']);

        $this->validate($request, $this->rules, $this->messages);

        try {
            $employeeEloquent = new \App\Employee();

            $employeeEloquent->first_name = $employee['first_name'];
            $employeeEloquent->last_name = $employee['last_name'];
            $employeeEloquent->gender = $employee['gender'];

            $employeeEloquent->save();
            $id = $employeeEloquent->id;

            session()->flash('message', 'New employee has been inserted');

            return redirect()->to($this->indexPage);
        } catch (Exception $e) {
            abort(500, 'Internal Server Error');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $employee = \App\Employee::find($id);

        return view('employees.show', compact('employee'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee = \App\Employee::find($id);

        return view('employees.edit', compact('employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $employee = request()->except(['_token', '_method']);

        $this->validate($request, $this->rules);

        try {
            $employeeEloquent = \App\Employee::find($id);

            $employeeEloquent->first_name = $employee['first_name'];
            $employeeEloquent->last_name = $employee['last_name'];
            $employeeEloquent->gender = $employee['gender'];

            $employeeEloquent->save();

            session()->flash('message', 'New employee has been updated');

            return redirect($this->indexPage);
        } catch (Exception $e) {
            abort(500, 'Internal Server Error');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $employeeEloquent = \App\Employee::find($id);
            $employeeEloquent->delete();

            session()->flash('message', 'New employee has been deleted');

            return redirect($this->indexPage);

        } catch (Exception $e) {
            abort(500, 'Internal Server Error');
        }
    }
}
