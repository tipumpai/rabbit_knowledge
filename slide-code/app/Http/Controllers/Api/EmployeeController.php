<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $queries = request()->query();

        if (isset($queries['gender'])) {
            $employees = \DB::table('employees')
                ->where('gender', strtoupper($queries['gender']))
                ->get();
        } else {
            $employees = \DB::table('employees')
                ->get();
        }

        return response()->json($employees);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $employee = request()->all();

        try {
            \DB::table('employees')->insert($employee);

            return response(['message' => 'success'], 200);
        } catch(Exception $e) {
            return response(['message' => $e], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $employee = \DB::table('employees')
            ->where('id', $id)
            ->first();

        return response()->json($employee);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $employee = request()->all();

        try {
            \DB::table('employees')
                ->where('id', $id)
                ->update($employee);

            return response(['message' => 'success'], 200);
        } catch (Exception $e) {
            return response(['message' => $e], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            \DB::table('employees')
                ->where('id', $id)
                ->delete();

            return response(['message' => 'success'], 200);
        } catch (Exception $e) {
            return response(['message' => $e], 500);
        }
    }
}
