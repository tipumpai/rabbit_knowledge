<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    public function getGenderAttribute($value)
    {
        return $value;
    }

    public function getFullGenderAttribute($value)
    {
        return ($this->attributes['gender'] === 'M') ? 'Male' : 'Female';
    }

    public function setGenderAttribute($value)
    {
        return $this->attributes['gender'] = ucfirst($value);
    }

    public function salaries()
    {
        return $this->hasMany('App\Salary');
    }
}
